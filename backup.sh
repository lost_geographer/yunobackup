#!/bin/bash

# First steps (warnig, the next lines are set to download script from 'dev' brach):
# 1. See full documentation: https://framagit.org/lost_geographer/yunobackup
# 2. Download script file and set permissions: curl -s https://framagit.org/lost_geographer/yunobackup/-/raw/dev/backup.sh -o backup.sh && chmod 700 backup.sh
# 3. Download configuration file, set permissions and edit values following documentation: curl -s https://framagit.org/lost_geographer/yunobackup/-/raw/dev/backup.conf -o backup.conf && chmod 600 backup.sh
# 4. Test script: ./backup.sh
# 5. Set cron file: /etc/cron.d/backup

# Below comment is for syntax check. The sourced file must match the configuration file name. It can be safely removed.
# shellcheck source=backup.conf

# 0 to 8 stages

# 0.1) Script initialization: Set core variables

# Where the configuration file is (by default it's in the same directory as the current script).
scriptConfiguration="backup.conf"

# Used to test if the script is up-to-date with the repository version.
scriptLocalVersion="0.4"

# 0.2) Script initialization: Set error and output handlers

set -Eeuo pipefail # The script will exit at any command failure (not only when it's planned: `output_handler "ERROR"`).

output_handler() {
	local logLevel
	local logMessage
	local logTimestamp
	local lineNumber

	logLevel="$1"
	logMessage="$2"
	logTimestamp="$(date '+%Y-%m-%d %H:%M:%S')"
	lineNumber="${BASH_LINENO[0]}"	# Get the current line number

	case $logLevel in
		"FATAL")
			echo "$logTimestamp: FATAL: line $lineNumber: $logMessage" >&2
			exit 1
			;;
		"ERROR")
			echo "$logTimestamp: ERROR: line $lineNumber: $logMessage" >&2
			send_mail "$logTimestamp: ERROR: line $lineNumber: $logMessage"
			exit 1
			;;
		"WARNING")
			if [[ "$verbosity" == "warning" ]] || [[ "$verbosity" == "notice" ]] || [[ "$verbosity" == "info" ]]; then
				echo "$logTimestamp: WARNING: line $lineNumber: $logMessage"
				send_mail "$logTimestamp: WARNING: line $lineNumber: $logMessage"
			fi
			;;
		"NOTICE")
			if [[ "$verbosity" == "notice" ]] || [[ "$verbosity" == "info" ]]; then
				echo "$logTimestamp: NOTICE: line $lineNumber: $logMessage"
			fi
			;;
		"INFO")
			if [[ "$verbosity" == "info" ]]; then
				echo "$logTimestamp: INFO: line $lineNumber: $logMessage"
			fi
			;;
		*)
			echo "$logTimestamp: Unknown log level: line $lineNumber: $logMessage" >&2
			send_mail "$logTimestamp: ERROR: line $lineNumber: $logMessage"
			;;
	esac
}

unhandled_error() {
	output_handler "ERROR" "Unhandled error occurred at line ${BASH_LINENO[0]} with command: ${BASH_COMMAND}"
}

trap 'unhandled_error' ERR

# 0.3) Script initialization: load functions

check_url() {
	local url
	local response

	url="$1"
	response=$(curl -L -s -o /dev/null -w "%{http_code}" "$url") # `-L` follow redirections if any.

	if [[ "$response" -eq 200 ]]; then
		output_handler "INFO" "URL '$url' is reachable."
		return 0
	else
		output_handler "ERROR" "URL '$url' is not reachable. Error: $response."
		return 1
	fi
}

check_program() {
	if command -v "$1" >/dev/null 2>&1; then # `-v` print command path if exists. If no path exists, then error.
		output_handler "INFO" "$1 is installed."
		return 0
	else
		output_handler "WARNING" "$1 is not installed."
		return 1
	fi
}

format_size() {
	local size

	size=$1

	if (( size < 1024 )); then
		printf "%d B" "$size"
	elif (( size < 1048576 )); then
		printf "%.2f KiB" "$(bc <<< "scale=2; $size / 1024" | sed "s/\./$decimalSeparator/")"
	elif (( size < 1073741824 )); then
		printf "%.2f MiB" "$(bc <<< "scale=2; $size / 1048576" | sed "s/\./$decimalSeparator/")"
	else
		printf "%.2f GiB" "$(bc <<< "scale=2; $size / 1073741824" | sed "s/\./$decimalSeparator/")"
	fi
}

check_age_format() {
	local age

	age="$1"

	if [[ "$age" =~ ^[1-9][0-9]*(ms|s|m|h|d|w|M|y)$ ]]; then
		output_handler "INFO" "'$1' age format is valid."
	else
		output_handler "ERROR" "'$1' age format is not valid."
	fi
}

check_conf() {
	local confValue
	local confName

	confName="$1"
	confValue="${!1}"

	if [[ -z "${confValue// }" ]]; then 
		output_handler "NOTICE" "'$confName' not set."
	else
		output_handler "NOTICE" "'$confName' set to '$confValue'."
	fi
}

send_mail() {
	local message

	message="$1"
	receiver=root@$(hostname)

	if [[ "${mailSend,,}" == "true" ]]; then
		output_handler "INFO" "Will send $logPath to $receiver by mail."
		if [[ "${mailEncryption,,}" == "true" ]]; then
			output_handler "NOTICE" "User choosed to send an ecrypted mail."

			if command -v gpg >/dev/null 2>&1 && [[ -f /usr/sbin/sendmail ]]; then
				output_handler "INFO" "'gpg' and 'sendmail' programs are installed."

				if [[ $(gpg --list-keys --with-colons --fixed-list-mode "$receiver") ]]; then output_handler "INFO" "Public key for receiver exists."; else output_handler "FATAL" "Public key for receiver does not exists."; fi

				logContent=$(cat "$logPath")
				bodyMessage=$(echo -e "\n$message\n\nA log has been appended below:\n\n$logContent")

				boundary1=$(openssl rand -hex 16 | base64 | tr -dc 'a-zA-Z0-9' | head -c 32)
				boundary2=$(openssl rand -hex 16 | base64 | tr -dc 'a-zA-Z0-9' | head -c 32)

				encapsulated_mail=$(cat <<EOF
Content-Type: multipart/mixed; boundary="$boundary2";
 protected-headers="v1"
Subject: Backup alert
From: $receiver
To: $receiver

--$boundary2
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: base64

$(echo "$bodyMessage" | base64)

--$boundary2--
EOF
				)
				
				encrypted_mail=$(echo -e "$encapsulated_mail" | gpg --encrypt --recipient "$receiver" --armor)

				(cat <<EOF
Date: $(date -R)
MIME-Version: 1.0
From: root@$(hostname)
To: $receiver
Subject: ...
Content-Type: multipart/encrypted;
 protocol="application/pgp-encrypted";
 boundary="$boundary1"

This is an OpenPGP/MIME encrypted message (RFC 4880 and 3156)
--$boundary1
Content-Type: application/pgp-encrypted
Content-Description: PGP/MIME version identification

Version: 1

--$boundary1
Content-Type: application/octet-stream; name="encrypted.asc"
Content-Description: OpenPGP encrypted message
Content-Disposition: inline; filename="encrypted.asc"

$encrypted_mail

--$boundary1--
EOF
				) | /usr/sbin/sendmail -t -oi
				output_handler "INFO" "An encrypted mail has been sent."
			else
				output_handler "FATAL" "'gpg' and/or 'sendmail' are not installed. Won't be able to send encrypted mail."
			fi
		else
			output_handler "NOTICE" "User choosed plain mail."
			if command -v mail >/dev/null 2>&1; then
				output_handler "INFO" "'mail' program is installed."
				echo "$message A log has been attached." | mail -s "Unexpected backup event ヽ(ﾟДﾟ)ﾉ" -A "$logPath" "$receiver"
				output_handler "INFO" "An plain mail as been sent."
			else
				output_handler "WARNING" "'mail' program is not installed. Won't be able to send $logPath to $receiver by mail if any error."
			fi
		fi
	else
		output_handler "NOTICE" "Because of user override, won't send $logPath to $receiver by mail."
	fi
}

# 0.3) Script initailization : load configuration file

originalDirectory=$(pwd)
echo "$(date '+%Y-%m-%d %H:%M:%S'): INFO: Current working directory is $originalDirectory"

if cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null; then
    scriptDirectory=$(pwd)
    echo "$(date '+%Y-%m-%d %H:%M:%S'): INFO: Working directory changed to $scriptDirectory"
else
    echo "$(date '+%Y-%m-%d %H:%M:%S'): ERROR: Couldn't change to script working directory."
    exit 1
fi

if [[ -f "$scriptConfiguration" && -s "$scriptConfiguration" && -r "$scriptConfiguration" ]]; then
	if . "$scriptConfiguration"; then output_handler "INFO" "'$scriptConfiguration' has been successfully loaded."; else output_handler "FATAL" "An error occurred while loading '$scriptConfiguration'."; fi
elif [[ -f "$scriptConfiguration" && -s "$scriptConfiguration" ]]; then
	output_handler "FATAL" "$scriptConfiguration exists and is not empty, but it is not readable. Please change read permission for '$scriptConfiguration'."
else
	output_handler "FATAL" "$scriptConfiguration is empty or does not exist or is not a regular file. Please check or try to download a new one at 'curl -s https://framagit.org/lost_geographer/yunobackup/-/raw/stable/$scriptConfiguration > $scriptConfiguration'."
fi

# 0.4) Script initialization: check privileges

if [[ "$EUID" -ne 0 ]]; then
	if [[ "${ifNoRootExit,,}" == "true" ]]; then
			output_handler "ERROR" "This script must be run as root"
		elif [[ "${ifNoRootExit,,}" == "false" ]]; then
			output_handler "WARNING" "This script must be run as root"
		else
		output_handler "ERROR" "Can't understand your 'ifNoRootExit' choice in '$scriptConfiguration'. It must be 'true' or 'false'. Exiting script."
		fi
fi

scriptFile=$(basename "$0")
permissions=$(stat -L -c "%a" "$scriptFile")

if [[ "$permissions" -eq 700 ]]; then
	output_handler "INFO" "$scriptFile permissions set to '700'. Only the owner has the right to read, write and execute the file."
else
	output_handler "WARNING" "$scriptFile permissions are not set to '700'. Will try to change $scriptFile permissions."
	if chmod 700 "$scriptFile"; then output_handler "INFO" "$scriptFile permissions successfully set to '700'. Only the owner has the right to read, write and execute the file."; else output_handler "ERROR" "Could not set $scriptFile permissions to '700'."; fi
fi

# 0.5) Script initialization: check old logs, then start logging

if [[ -f "$logPath" ]] && [[ -r "$logPath" ]];	then
	if grep -q "ERROR:" "$logPath"; then
		output_handler "FATAL" "Error(s) found on the last backup log '$logPath'. Please check and clean the log."
	else
		logStatus="info"
	fi
else
	logStatus="warning"
fi

LOGFILE="$logPath"
exec &> "$LOGFILE"

output_handler "INFO" "### STAGE 0/8 SCRIPT INITIALIZATION ###"

if [[ "$logStatus" == "info" ]]; then
	output_handler "INFO" "No issues found on the last backup log in '$logPath'. File overwritten."
elif [[ "$logStatus" == "warning" ]]; then
	output_handler "WARNING" "No backup file found. A new one was created at '$logPath'."
else
	output_handler "FATAL" "Can't read last log status."
fi

output_handler "INFO" "Current working directory has been set to '$scriptDirectory'."

# 0.6) Script initialization: check dependencies

if check_program curl; then
	installed_version=$(curl --version | awk '{print $2}' | head -n 1)
	if latest_version=$(apt-cache show curl | grep -m1 "Version:" | awk '{print $2}' | awk -F- '{print $1}'); then
		if [[ "$installed_version" == "$latest_version" ]]; then
			output_handler "INFO" "You have the latest version of curl ($installed_version) installed."
		else
			output_handler "WARNING" "Your installed version of curl is $installed_version, but the latest version is $latest_version."
		fi
	else
		output_handler "WARNING" "Couldn't retrieve curl version."
	fi
else
	output_handler "ERROR" "curl is not installed."
fi

if check_program rclone; then
	if rcloneInstalledVersion=$(rclone --version | head -n 1 | cut -d ' ' -f2); then
		if check_url "https://downloads.rclone.org/version.txt"; then
			rcloneLatestVersion=$(curl -s https://downloads.rclone.org/version.txt | cut -d ' ' -f2) # This is not an error: $rcloneInstalledVersion and $rcloneLatestVersion `cut` pipes are the same, because the installed and remote versions are formatted identically.
			if [[ "$rcloneInstalledVersion" = "$rcloneLatestVersion" ]]; then
				output_handler "INFO" "You have the latest version of rclone ($rcloneInstalledVersion) installed."
			else
				output_handler "WARNING" "Your installed version of rclone is $rcloneInstalledVersion, but the latest version is $rcloneLatestVersion."
				if rclone selfupdate; then output_handler "INFO" "rclone update successful."; else output_handler "ERROR" "rclone update failed."; fi
			fi
		else
			output_handler "ERROR" "Couldn't check rclone latest version."
		fi
	fi
else
	output_handler "ERROR" "rclone is not installed."
fi

if check_program yunohost; then
	installed_version=$(yunohost --version | awk '/version:/ {print $2; exit}')
	if latest_version=$(apt-cache policy yunohost | grep "Candidate" | awk '{print $2}'); then
		if [[ "$installed_version" == "$latest_version" ]]; then
			output_handler "INFO" "You have the latest version of yunohost ($installed_version) installed."
			else
			output_handler "WARNING" "Your installed version of yunohost is $installed_version, but the latest version is $latest_version."
		fi
	else
		output_handler "ERROR" "Couldn't check yunohost latest version."
	fi
else
	output_handler "ERROR" "yunohost is not installed."
fi

# 0.7) Script initialization: check for script and configuration updates

check_conf "scriptAutoUpdate"
check_conf "scriptAutoRemove"
check_conf "rcloneConfigurationPathOverride"
check_conf "rcloneRemoteStorageOverride"
check_conf "rcloneRemoteStorageDirectory"
check_conf "localBackupDirectory"
check_conf "sizeVariationThreshold"
check_conf "backupFilesCountCleanTrigger"
check_conf "backupFilesLocalAge"
check_conf "backupFilesRemoteAge"
check_conf "mailSend"
check_conf "mailEncryption"
check_conf "logPath"
check_conf "verbosity"
check_conf "scriptRemoteRepository"
check_conf "scriptRemoteBranch"
check_conf "ifNoRootExit"

check_url "$scriptRemoteRepository/$scriptRemoteBranch/backup.sh"
curl -s "$scriptRemoteRepository/$scriptRemoteBranch/backup.sh" -o "$scriptFile".new

scriptRemoteVersion=$(grep -m 1 'scriptLocalVersion=' "$scriptFile".new | awk -F '=' '{gsub(/"/, "", $2); print $2}')

if [[ "$scriptLocalVersion" == "$scriptRemoteVersion" ]]; then
	output_handler "INFO" "You have the latest version of $scriptFile ($scriptLocalVersion) installed."
	rm "$scriptFile".new
else
	output_handler "WARNING" "Your installed version of $scriptFile is $scriptLocalVersion, but the latest version is $scriptRemoteVersion."
	if [[ -z "${scriptAutoUpdate// }" ]]; then
		output_handler "ERROR" "Please add a value for 'scriptAutoUpdate' in  '$scriptConfiguration'."
	else
		if [[ "${scriptAutoUpdate,,}" == "true" ]]; then
			output_handler "NOTICE" "'scriptAutoUpdate' is set to 'true'. Will try to update '$scriptFile' and '$scriptConfiguration' (configuration values will be preserved)."
			if mv -f "$scriptFile" "$scriptFile".old && mv -f "$scriptFile".new "$scriptFile" && chmod 700 "$scriptFile"; then
				output_handler "INFO" "$scriptFile has been updated."
			else
				output_handler "ERROR" "$scriptFile update failed."
			fi

			if cmp -s "$scriptConfiguration" "$scriptConfiguration.new"; then
				output_handler "NOTICE" "No differences found between the new and the old $scriptConfiguration files. Skipping $scriptConfiguration update."
			else
				output_handler "NOTICE" "Some differences found between the new and the old $scriptConfiguration files. Will update $scriptConfiguration."

				check_url "$scriptRemoteRepository/$scriptRemoteBranch/backup.conf"
				curl -s "$scriptRemoteRepository/$scriptRemoteBranch/backup.conf" -o $scriptConfiguration.new
				
				tempFile=$(mktemp)

				cp "$scriptConfiguration" "$scriptConfiguration".old

				if (while IFS= read -r line; do
					if [[ "$line" =~ ^[a-zA-Z_][a-zA-Z0-9_]*= ]]; then
						key="${line%%=*}"

						if grep -q "^$key=" "$scriptConfiguration.old"; then
							value_old=$(grep "^$key=" "$scriptConfiguration.old")
							echo "$key=${value_old#*=}" >> "$tempFile"
						else
							 echo "$line" >> "$tempFile"
						fi
					else
						 echo "$line" >> "$tempFile"
					fi
				done < "$scriptConfiguration".new); then
					mv -f "$tempFile" "$scriptConfiguration" && chmod 600 $scriptConfiguration
					rm "$scriptConfiguration".new
				else
					rm "$tempFile"
					mv -f "$scriptFile".old "$scriptFile" && chmod 700 "$scriptFile"
					output_handler "ERROR" "Values of the old configuration file couldn't be loaded in the new one. Old configuration file is preserved and old script has been restored. Please check $scriptConfiguration.old file and add the old values manually to new configuration file located at $scriptConfiguration.new. Eventually, download a new configuration file at $scriptRemoteRepository/$scriptRemoteBranch/$scriptConfiguration (check if the branch is correct)."					
				fi

				output_handler "INFO" "$scriptConfiguration has been updated (your old values have been preserved)."
			fi

			output_handler "NOTICE" "New '$scriptFile' and '$scriptConfiguration' files will be loaded on next run."

			if [[ -z "${scriptAutoRemove// }" ]]; then
				output_handler "ERROR" "Please add a value for 'scriptAutoRemove' in  '$scriptConfiguration'."
			else
				if [[ "${scriptAutoRemove,,}" == "true" ]]; then
					output_handler "NOTICE" "'scriptAutoRemove' is set to 'true'. Old versions of $scriptFile and $scriptConfiguration will be removed."
					if rm "$scriptFile".old; then
						output_handler "INFO" "$scriptFile.old has been removed."
					else
						output_handler "ERROR" "$scriptFile.old removal failed."
					fi
					if rm "$scriptConfiguration.old"; then
						output_handler "INFO" "$scriptConfiguration.old has been removed."
					else
						output_handler "ERROR" "$scriptConfiguration.old removal failed."
					fi
				elif [[ "${scriptAutoRemove,,}" == "false" ]]; then
					output_handler "NOTICE" "'scriptAutoRemove' is set to 'false'. Old versions of $scriptFile and $scriptConfiguration will be kept in $scriptFile.old and $scriptConfiguration.old."
				else
					output_handler "ERROR" "Can't understand your 'scriptAutoRemove' choice in '$scriptConfiguration'. It must be 'true' or 'false'. Old files have been kept."
				fi
			fi
		elif [[ "${scriptAutoUpdate,,}" == "false" ]]; then
			output_handler "NOTICE" "'scriptAutoUpdate' is set to 'false'. Will not update '$scriptFile' and '$scriptConfiguration'."
			rm "$scriptFile".new
		else
			output_handler "ERROR" "Couldn't interpret 'scriptAutoUpdate'. Please check '$scriptConfiguration'."
			rm "$scriptFile".new
		fi
	fi
fi

# 1.1) Process initialization: check local backup directory

output_handler "INFO" "### STAGE 1/8 BACKUP INITIALIZATION ###"

if [[ -z "${localBackupDirectory// }" ]]; then
	output_handler "ERROR" "Please add a value for 'localBackupDirectory' in  '$scriptConfiguration'."
else
	if [[ -d "$localBackupDirectory" ]] && [[ -r "$localBackupDirectory" ]] && [[ -w "$localBackupDirectory" ]]; then
		cd "$localBackupDirectory" && output_handler "INFO" "Backup directory '$localBackupDirectory' exists, is readable and writable."
	else
		output_handler "ERROR" "Backup directory '$localBackupDirectory' missing. Suggestion: check read/write privileges for the backup directory or misspelled path in '$scriptConfiguration'."
	fi
fi

# 1.2) Process initialization: check rclone configuration

if [[ -z "${rcloneConfigurationPathOverride// }" ]]; then # Test if `$rcloneConfigurationPathOverride` is empty (-z). `// ` strip spaces.
	output_handler "WARNING" "rclone configuration file path override in $scriptConfiguration is empty. You shoud set one manually."
	output_handler "INFO" "Will lookup for rclone configuration file and automatically set its path. The process will take some time. Patience..."
	mapfile -t file_paths < <(find / -name "rclone.conf" 2>/dev/null || true) # || true is to avoid the script to exit prematurely because of `set -Eeuo pipefail`
	num_paths=${#file_paths[@]}

	if [ "$num_paths" -eq 0 ]; then
		output_handler "ERROR" "'rclone.conf file not found on your system. Please specify a path for 'rclone.conf' in '$scriptConfiguration' file (set a 'rcloneConfigurationPathOverride' value)."
	elif [ "$num_paths" -eq 1 ]; then
		output_handler "INFO" "File exists at: ${file_paths[0]}"
		rcloneConfigurationPath=${file_paths[0]}
	else
		output_handler "WARNING" "File exists at multiple locations:"
		for path in "${file_paths[@]}"; do
			echo "- $path"
		done
		output_handler "ERROR" "Please set a unique path for 'rclone.conf' in '$scriptConfiguration' file."
	fi
else
	output_handler "INFO" "rclone configuration file path override in $scriptConfiguration is not empty. Reading rclone configuration file path override from $scriptConfiguration."
	if [[ $rcloneConfigurationPathOverride =~ [[:space:]] ]]; then
		output_handler "WARNING" "rclone configuration file path override contains spaces, but it's not necessarily an error."
	fi
	rcloneConfigurationPath="$rcloneConfigurationPathOverride"
	if [[ -f "$rcloneConfigurationPath" ]]; then
		output_handler "INFO" "rclone configuration file path has been manually set to '$rcloneConfigurationPath' and exists."
	else
		output_handler "ERROR" "You set an override in $scriptConfiguration, but there is an error: '$rcloneConfigurationPathOverride' (check for mispelling and for rlcone.conf actual existence)."
	fi
fi

if [[ -s "$rcloneConfigurationPath" ]] && [[ -r "$rcloneConfigurationPath" ]]; then
	output_handler "INFO" "'rclone.conf' is not empty and readable."
else
	output_handler "ERROR" "'rclone.conf' empty and/or not readable."
fi

if [[ -z "${rcloneRemoteStorageOverride// }" ]]; then
	output_handler "INFO" "rclone remote storage override in $scriptConfiguration is empty. Will auto-set rclone remote storage from rclone.conf (if only ONE remote is configured)."
	numRemoteStorages=$(grep -c -E '^\[.*\]$' "$rcloneConfigurationPath")
	if [[ "$numRemoteStorages" -eq 1 ]]; then
		rcloneRemoteStorage=$(grep -E '^\[.*\]$' "$rcloneConfigurationPath" | sed -e 's/^\[\(.*\)\]$/\1/')
		rcloneRemoteStorageSource="auto"
		output_handler "INFO" "rclone remote storage has been automatically set to '$rcloneRemoteStorage'."
	else
		output_handler "ERROR" "Multiple remote storages found in '$rcloneConfigurationPath'. Please specify the desired remote storage manually in $scriptConfiguration (rcloneRemoteStorageOverride)."
	fi
else
	rcloneRemoteStorage="$rcloneRemoteStorageOverride"
	output_handler "INFO" "rclone remote storage override in $scriptConfiguration is not empty. Reading rclone configuration file path override from $scriptConfiguration."
	if [[ $rcloneRemoteStorageOverride =~ [[:space:]] ]]; then
		output_handler "ERROR" "rclone configuration file path override contains spaces."
	fi
	rcloneRemoteStorageSource="override"
	rcloneRemoteStorage=$rcloneRemoteStorageOverride
	output_handler "INFO" "rclone remote storage has been manually set to '$rcloneConfigurationPath'."
	if grep -q "\[$rcloneRemoteStorage\]" "$rcloneConfigurationPath"; then
		output_handler "INFO" "The remote storage '$rcloneRemoteStorage' exists in '$rcloneConfigurationPath'."
	else
		if [[ "$rcloneRemoteStorageSource" == "override" ]]; then
			output_handler "ERROR" "You set a rclone remote storage override in $scriptConfiguration in $scriptConfiguration, but there is an error with its name (check its existence or spelling)."
		else
			output_handler "ERROR" "The auto-set rclone remote storage override is wrong (check rclone.conf existence, path, read permission and content). You might want to specify the name in $scriptConfiguration. Specify the name in 'rcloneRemoteStorageOverride'."
		fi
		output_handler "ERROR" "The remote storage '$rcloneRemoteStorage' does not exist in '$rcloneConfigurationPath'. Cannot determine what went wrong."
	fi
fi

if [[ -z "${rcloneRemoteStorageDirectory// }" ]]; then
	output_handler "INFO" "rclone remote storage directoty in $scriptConfiguration is empty. The backup will be sent to the root directory of remote storage '$rcloneRemoteStorage'."
else
	output_handler "INFO" "rclone remote storage directoty in $scriptConfiguration is not empty. The backup will be sent to '$rcloneRemoteStorageDirectory' directory of remote storage '$rcloneRemoteStorage'."
fi

# 1.3) Process initialization: check remote storage connection

if rcloneRemoteUrl=$(awk -v sec="$rcloneRemoteStorage" -v par="url" '/^\[/{flag=0} /^\['"$rcloneRemoteStorage"'\]/{flag=1;next} flag && $1==par{print $3}' "$rcloneConfigurationPath" | sed 's#^\(https*://[^/]*\)/.*#\1#'); then
	output_handler "INFO" "Remote storage [$rcloneRemoteStorage] url has been automatically set to: '$rcloneRemoteUrl'."
else
	output_handler "ERROR" "Couldn't extract the remote storage [$rcloneRemoteStorage] url from '$rcloneConfigurationPath'. Please check '$rcloneConfigurationPath'."
fi

check_url "$rcloneRemoteUrl"

# 2) Backup creation: local backup creation

output_handler "INFO" "### STAGE 2/8 LOCAL BACKUP CREATION ###"

output_handler "NOTICE" "Starting yunohost backup creation process, switching to yunohost raw output if any. The process will take some time. Patience..."
if yunohost backup create; then output_handler "INFO" "Backup creation successful."; else output_handler "ERROR" "Backup creation failed."; fi

# 3.1) Backup verification: basic integrity check

output_handler "INFO" "### STAGE 3/8 LOCAL BACKUP VERIFICATION ###"

if newBackupName=$(find . -maxdepth 1 -type f -name '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9].tar' | sort -r | head -n 1); then
  output_handler "INFO" "Retrieved new backup name: '$newBackupName'."
else
  output_handler "ERROR" "Couldn't retrieve new backup name."
fi

if tar -tf "$newBackupName" >/dev/null; then output_handler "INFO" "The new backup '$newBackupName' files were correctly listed."; else output_handler "ERROR" "The new backup '$newBackupName' files couldn't be correctly listed."; fi # `-t` list contents of specified archive `-f`.

# 3.2) Backup verification: sizes of last two local backups are compared

# Use locale fomat
decimalSeparator=$(locale decimal_point)

output_handler "NOTICE" "The system is configured to use the '$decimalSeparator' as decimal separator."

if newBackupSize=$(stat -c "%s" "$newBackupName"); then output_handler "INFO" "New backup size is '$(format_size "$newBackupSize")'."; else output_handler "ERROR" "New backup size couldn't be calculated."; fi # `-c` Display specified property: "%s" size, in bytes.

if backupFilesCount=$(find . -maxdepth 1 -type f -name '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9].tar' | wc -l); then
  output_handler "INFO" "Local backup TAR files count: '$backupFilesCount'."
else
  output_handler "ERROR" "Local backup TAR files couldn't be counted."
fi

if [[ "$backupFilesCount" -gt 1 ]]; then
	if penultimateBackupName=$(find . -maxdepth 1 -type f -name '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9].tar' | sort -r | head -n 2 | tail -n 1); then
  	output_handler "INFO" "Retrieved penultimate backup name: '$penultimateBackupName'."
	else
  	output_handler "ERROR" "Couldn't retrieve penultimate backup name."
	fi

	if penultimateBackupSize=$(stat -c "%s" "$penultimateBackupName"); then	output_handler "INFO" "Penultimate backup size is '$(format_size "$penultimateBackupSize")'."; else output_handler "ERROR" "Penultimate backup size couldn't be calculated."; fi

	if sizeVariationPercentage=$(((newBackupSize - penultimateBackupSize) * 100 / penultimateBackupSize)); then output_handler "INFO" "Size variation is '$sizeVariationPercentage %'."; else output_handler "ERROR" "Size variation percentage couldn't be calculated."; fi

	if [[ -z "${sizeVariationThreshold// }" ]]; then
	output_handler "ERROR" "Please add a value for 'sizeVariationThreshold' in  '$scriptConfiguration'."
	else
		if [[ "$sizeVariationThreshold" =~ ^-?([1-9][0-9]*|0)$ ]]; then # Test if it's an integer (negative or 0 are accepted).
			sizeVariationThresholdNegative=$((sizeVariationThreshold * -1)) &&	output_handler "INFO" "Critical size variation threshold is '$sizeVariationThreshold%'."
		else
			output_handler "ERROR" "The variation threshold is not an integer. Please add or edif value in $scriptConfiguration."
		fi

		if [[ "$sizeVariationPercentage" -le "$sizeVariationThreshold" ]] && [[ "$sizeVariationPercentage" -ge "$sizeVariationThresholdNegative" ]]; then
			output_handler "INFO" "Given the threshold, the size variation between $newBackupName and $penultimateBackupName backup files seems correct."
		else
		output_handler "WARNING" "Given the threshold, the size variation between $newBackupName and $penultimateBackupName backup files seems incorrect."
		fi

		if newBackupInfo=$(find . -maxdepth 1 -type f -name '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9].info.json' | sort -r | head -n 1); then
 		 output_handler "INFO" "Retrieved new backup info file: '$newBackupInfo'."
		else
		  output_handler "ERROR" "Couldn't retrieve new backup info file."
		fi

		if penultimateBackupInfo=$(find . -maxdepth 1 -type f -name '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9].info.json' | sort -r | head -n 2 | tail -n 1); then
			output_handler "INFO" "Retrieved penultimate backup info file: '$penultimateBackupInfo'."
		else
			output_handler "ERROR" "Couldn't retrieve penultimate backup info file."
		fi

		newBackupApps=$(jq -r '.size_details.apps | to_entries[] | "\(.key)=\(.value)"' "$newBackupInfo")

		newBackupArray=()
		while IFS= read -r line; do
		  newBackupArray+=("$line")
		done <<< "$newBackupApps"

		if [[ ${#newBackupArray[@]} -eq 0 ]]; then
		  output_handler "ERROR" "Failed to read new backup apps into an array."
		else
		  output_handler "INFO" "Successfully read new backup apps into an array."
		fi

		penultimateBackupApps=$(jq -r '.size_details.apps | to_entries[] | "\(.key)=\(.value)"' "$penultimateBackupInfo")

		penultimateBackupArray=()
		while IFS= read -r line; do
 		 penultimateBackupArray+=("$line")
		done <<< "$penultimateBackupApps"

		if [[ ${#penultimateBackupArray[@]} -eq 0 ]]; then
  		output_handler "ERROR" "Failed to read penultimate backup apps into an array."
		else
		  output_handler "INFO" "Successfully read penultimate backup apps into an array."
		fi

		addedAppsArray=()
		removedAppsArray=()

		for newApp in "${newBackupArray[@]}"; do
			appName=$(echo "$newApp" | cut -d '=' -f 1)
			newSize=$(echo "$newApp" | cut -d '=' -f 2)

			found=false
			for penultimateApp in "${penultimateBackupArray[@]}"; do
				penultimateAppName=$(echo "$penultimateApp" | cut -d '=' -f 1)
				penultimateSize=$(echo "$penultimateApp" | cut -d '=' -f 2)

				if [ "$appName" = "$penultimateAppName" ]; then
					sizeDifference=$((newSize - penultimateSize))
					sizeDifferencePercentage=$(((newSize - penultimateSize) * 100 / penultimateSize))
					formattedSizeDifference=$(format_size "$sizeDifference")
					output_handler "INFO" "App: $appName, Size Difference: $formattedSizeDifference ($sizeDifferencePercentage %)"

					if [[ "$sizeDifferencePercentage" -ge "$sizeVariationThreshold" ]] && [[ "$sizeDifferencePercentage" -le "$sizeVariationThresholdNegative" ]]; then
						output_handler "WARNING" "Given the threshold, the size variation of $appName seems incorrect."
					fi

					found=true
					break
				fi
			done

			if ! $found; then
				addedAppsArray+=("$appName")
			fi
		done

		for penultimateApp in "${penultimateBackupArray[@]}"; do
			penultimateAppName=$(echo "$penultimateApp" | cut -d '=' -f 1)
			found=false

			for newApp in "${newBackupArray[@]}"; do
				appName=$(echo "$newApp" | cut -d '=' -f 1)

				if [ "$appName" = "$penultimateAppName" ]; then
					found=true
					break
				fi
			done

			if ! $found; then
				removedAppsArray+=("$penultimateAppName")
			fi
		done

		if [[ ${#addedAppsArray[@]} -gt 0 ]]; then
			output_handler "WARNING" "Some apps were added in the last backup."
			for addedApp in "${addedAppsArray[@]}"; do
				output_handler "NOTICE" "- $addedApp"
			done
		fi

		if [[ ${#removedAppsArray[@]} -gt 0 ]]; then
			output_handler "WARNING" "Some apps were removed in the last backup."
			for removedApp in "${removedAppsArray[@]}"; do
				output_handler "NOTICE" "- $removedApp"
			done
		fi
	fi
fi

# 4) Backup to remote: backup is sent to remote storage
# All files "." are considered for copy so that the JSON file linked to each backup (backup description for yunohost program) are also sent to remote storage.

output_handler "INFO" "### STAGE 4/8 LOCAL BACKUP COPY ###"

output_handler "NOTICE" "Starting rclone backup copy process, switching to rclone raw output if any. The process will take some time. Patience..."

if rclone --config "$rcloneConfigurationPath" copy . "$rcloneRemoteStorage":"$rcloneRemoteStorageDirectory"; then
	output_handler "INFO" "New backup files copy to remote successful."
else
	output_handler "ERROR" "New backup files copy to remote failed."
fi

# `--config "$rcloneConfigurationPath"` flag is because since the script is run as root, by default it will look for the rclone configuration file located in ROOT home directory (/root/.config/rclone/rclone.conf), and we might not want this.

# 5) Remote backup integrity checked against local copy

output_handler "INFO" "### STAGE 5/8 REMOTE BACKUP VERIFICATION ###"

output_handler "INFO" "Will check remote backup files integrity. Switching to rclone raw output if any."
if rclone --config "$rcloneConfigurationPath" check --one-way "$newBackupName" "$rcloneRemoteStorage":"$rcloneRemoteStorageDirectory"; then
	output_handler "INFO" "Local and remote backup files check successful."
else
	output_handler "ERROR" "Local and remote backup files check failed."
fi

# It will check that files in the source match the files in the destination, not the other way around (`--one-way` flag). It compares sizes and hashes (MD5 or SHA1) and logs a report of files that don't match. It doesn't alter the source or destination. More information at: https://rclone.org/commands/rclone_check/

# 6) Backup cleaning: "old" backups files are deleted if there are more than a specific value (see $scriptConfiguration)

output_handler "INFO" "### STAGE 6/8 OLD BACKUPS CLEANING ###"

if [[ -z "${backupFilesCountCleanTrigger// }" ]]; then
	output_handler "ERROR" "Please add a value for 'backupFilesCountCleanTrigger' in  '$scriptConfiguration'."
else
	if [[ "$backupFilesCount" -gt "$backupFilesCountCleanTrigger" ]]; then
		if [[ -z "${backupFilesLocalAge// }" ]]; then
			output_handler "ERROR" "Please add a value for 'backupFilesLocalAge' in  '$scriptConfiguration'."
		else
			if check_age_format "$backupFilesLocalAge"; then
				if rclone --config "$rcloneConfigurationPath" delete --min-age "$backupFilesLocalAge" .; then output_handler "INFO" "Old local backup files delete successful."; else output_handler "ERROR" "Old local backup files delete failed"; fi
			fi
		fi
		if [[ -z "${backupFilesRemoteAge// }" ]]; then
			output_handler "ERROR" "Please add a value for 'backupFilesRemoteAge' in  '$scriptConfiguration'."
		else
			if check_age_format "$backupFilesRemoteAge"; then
				if rclone --config "$rcloneConfigurationPath" delete --min-age "$backupFilesRemoteAge" "$rcloneRemoteStorage":"$rcloneRemoteStorageDirectory"; then output_handler "INFO" "Old remote backup files delete successful."; else output_handler "ERROR" "Old remote backup files delete failed."; fi
			fi
		fi
	fi
fi

# 7) End of process

output_handler "INFO" "### STAGE 7/8 SPACE COUNT ###"

if localArchiveSize=$(rclone size . | tail -1 | awk '{print $3 " " $4}' | sed "s/\./$decimalSeparator/"); then
	localFreeSpace=$(rclone about . | tail -1 | awk '{print $2 " " $3}' | sed "s/\./$decimalSeparator/")
	output_handler "INFO" "Local archive size is $localArchiveSize. Free local space is $localFreeSpace."
else
	output_handler "ERROR" "Cannon calculate local archive size."
fi

if remoteArchiveSize=$(rclone size "$rcloneRemoteStorage":"$rcloneRemoteStorageDirectory" | tail -1 | awk '{print $3 " " $4}' | sed "s/\./$decimalSeparator/"); then
	remoteFreeSpace=$(rclone about "$rcloneRemoteStorage":"$rcloneRemoteStorageDirectory" | tail -1 | awk '{print $2 " " $3}' | sed "s/\./$decimalSeparator/")
	output_handler "INFO" "Remote archive size is $remoteArchiveSize. Free remote space is $remoteFreeSpace."
else
	output_handler "ERROR" "Cannot calculate local archive size."
fi

output_handler "INFO" "### STAGE 8/8 SCRIPT AND BACKUP PROCESS ENDED SUCCESSFULLY ###"

exit 0
